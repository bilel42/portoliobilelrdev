<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Projet;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class ProjetUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $projet = new Projet();
        $datetime= new DateTime(); 
        $categorie = new Categorie();
        $user = new User();
        $projet->setNom('nom')
               ->setEnCour(true)
               ->setDescription('description')
               ->setDateRealisation($datetime)
               ->setCreatedAt($datetime)
               ->setDuree($datetime)
               ->setPortfolio(true)
               ->setSlug('slug')
               ->setFile('file')
               ->addCategorie($categorie)
               ->setTarif(10,10)
               ->setUser($user);

        $this->assertTrue($projet->getNom() === 'nom');
        $this->assertTrue($projet->getDateRealisation() === $datetime);
        $this->assertTrue($projet->getDuree() === $datetime);
        $this->assertTrue($projet->getCreatedAt() === $datetime);
        $this->assertTrue($projet->isEnCour() === true);
        $this->assertTrue($projet->getDescription() === 'description');
        $this->assertTrue($projet->isPortfolio() === true);
        $this->assertTrue($projet->getSlug() === 'slug');
        $this->assertTrue($projet->getFile() === 'file');
        $this->assertTrue($projet->getTarif() == 10,10);
        $this->assertContains($categorie, $projet->getCategorie());
        $this->assertTrue($projet->getUser() === $user);

    }
    public function testIsFalse():void {
        $projet = new Projet();
        $datetime= new DateTime(); 
        $categorie = new Categorie();
        $user = new User();
        $projet->setNom('nom')
               ->setEnCour(true)
               ->setDescription('description')
               ->setDateRealisation($datetime)
               ->setCreatedAt($datetime)
               ->setDuree($datetime)
               ->setPortfolio(true)
               ->setSlug('slug')
               ->setFile('file')
               ->addCategorie($categorie)
               ->setTarif(10,10)
               ->setUser($user);
        $this->assertFalse($projet->getNom() === 'false');
        $this->assertFalse($projet->getDateRealisation() === new Datetime);
        $this->assertFalse($projet->getDuree() === new Datetime);
        $this->assertFalse($projet->getCreatedAt() === new Datetime);
        $this->assertFalse($projet->isEnCour() === false);
        $this->assertFalse($projet->getDescription() === 'false');
        $this->assertFalse($projet->isPortfolio() === false);
        $this->assertFalse($projet->getSlug() === 'false');
        $this->assertFalse($projet->getFile() === 'false');
        $this->assertFalse($projet->getTarif() == 12,12);
        $this->assertContains($categorie, $projet->getCategorie());
        $this->assertfalse($projet->getUser() === new User());

    }
    public function testIsEmpty()
    {
        $projet = new Projet();
        $this->assertEmpty($projet->getNom());
        $this->assertEmpty($projet->getDateRealisation());
        $this->assertEmpty($projet->getDuree());
        $this->assertEmpty($projet->getCreatedAt());
        $this->assertEmpty($projet->isEnCour());
        $this->assertEmpty($projet->getDescription());
        $this->assertEmpty($projet->isPortfolio());
        $this->assertEmpty($projet->getSlug());
        $this->assertEmpty($projet->getFile());
        $this->assertEmpty($projet->getTarif());
        $this->assertEmpty($projet->getCategorie());
        $this->assertEmpty($projet->getUser());

    }
}
