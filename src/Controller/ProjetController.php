<?php

namespace App\Controller;

use App\Entity\Projet;
use App\Repository\ProjetRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjetController extends AbstractController
{
    /**
     * @Route("/projets", name="projets")
     */
    public function projets(
        ProjetRepository $projetRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $projetRepository->findAll();

        $projets = $paginator->paginate(
            $data, /* query not result */
            $request->query->get('page', 1), /* page number */
            3 /* nombre de page */
        );

        return $this->render('projets/index.html.twig', [
            'projets' => $projets,
        ]);
    }
    /**
    * @Route ("realisations/{slug}", name="realisationDetails")
    */
    public function details(Projet $projet): Response
    {
        return $this->render('projets/details.html.twig', [
            'projet' => $projet,
        ]);
    }
}
