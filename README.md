## Portfolio Bilel REHAHLA Développeur Full-stack

## Environnement de développement

## Pré-requis

* php 7.4
* composer
* symfony cli
* Docker 
* docker-compose
* nodejs et npm


## lancer l'environnement de développement
composer install 
npm install 
npm run build 
docker-composer up -d
symfony serve -d

## Lancer des test
```bash
php bin/phpunit --testdox