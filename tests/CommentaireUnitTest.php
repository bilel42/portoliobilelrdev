<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Projet;
use DateTime;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new Blogpost();
        $projet = new Projet();

        $commentaire->setAuteur('auteur')
        ->setEmail('email@test.com')
        ->setCreatedAt($datetime)
        ->setContenu('contenu')
        ->setBlogpost($blogpost)
        ->setProjet($projet);

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getCreatedAt() === $datetime);
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
        $this->assertTrue($commentaire->getProjet() === $projet);

    }
    public function testIsFalse(): void 
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new Blogpost();
        $projet = new Projet();

        $commentaire->setAuteur('auteur')
        ->setEmail('email@test.com')
        ->setCreatedAt($datetime)
        ->setContenu('contenu')
        ->setBlogpost($blogpost)
        ->setProjet($projet);

        $this->assertFalse($commentaire->getAuteur() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false@test.com');
        $this->assertFalse($commentaire->getCreatedAt() === new Datetime());
        $this->assertFalse($commentaire->getBlogpost() === new Blogpost());
        $this->assertFalse($commentaire->getProjet() === new Projet());
   
    }

    public function testIsEmpty()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new Blogpost();
        $projet = new Projet();
        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getProjet());
        
        
    }
}
