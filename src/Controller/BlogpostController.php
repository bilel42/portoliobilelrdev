<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use App\Entity\Blogpost;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogpostController extends AbstractController
{
    /**
     * @Route("/actualites", name="actualites")
     */
    public function actualites(
        BlogpostRepository $blogpostRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $blogpostRepository->findAll();
        $actualites = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            3
        );
        return $this->render('actualites/index.html.twig', [
            'actualites' => $actualites,
        ]);
    }
    /**
     * @Route("blogpost/{slug}", name="actualiteDetail")
     */
    public function detail(Blogpost $blogpost): Response
    {
        return $this->render('actualites/detail.html.twig', [
            'blogpost' => $blogpost,
        ]);
    }
}
