<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AProposController extends AbstractController
{
    /**
     * @Route("/a-propos", name="aPropos")
     */
    public function index(UserRepository $developpeur): Response
    {
        return $this->render('a_propos/index.html.twig', [
            'developpeur' => $developpeur->getUser(),
        ]);
    }
}
