<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testTrue(): void
    {
        $user = new User();
        $user->setEmail('true@test.com')
        ->setPrenom('prenom')
        ->setNom('nom')
        ->setPassword('password')
        ->setAPropos('a propos')
        ->setLinkedin('linkedin');

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getPrenom() === 'prenom');
        $this->assertTrue($user->getNom() === 'nom');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getAPropos() === 'a propos');
        $this->assertTrue($user->getLinkedin() === 'linkedin');
    }

    public function testFalse() :void 
    {
        $user = new User();
        $user->setEmail('true@test.com')
        ->setPrenom('prenom')
        ->setNom('nom')
        ->setPassword('password')
        ->setAPropos('a propos')
        ->setLinkedin('linkedin');

        $this->assertfalse($user->getEmail() === 'false@test.com');
        $this->assertfalse($user->getPrenom() === 'false');
        $this->assertfalse($user->getNom() === 'false');
        $this->assertfalse($user->getPassword() === 'false');
        $this->assertfalse($user->getAPropos() === 'false');
        $this->assertfalse($user->getLinkedin() === 'false');
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getLinkedin());
    }
}
