<?php

namespace App\Controller;

use App\Repository\ProjetRepository;
use App\Repository\BlogpostRepository;
use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(ProjetRepository $projetRepository, BlogpostRepository $blogpostRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'projets' => $projetRepository->lastTree(),
            'blogposts' => $blogpostRepository->lastTree(),
            'categories' => $blogpostRepository->findAll(),
        ]);
    }
}
