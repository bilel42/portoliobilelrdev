<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Blogpost;
use App\Entity\Categorie;
use App\Entity\Projet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    public function load(ObjectManager $manager): void
    {
        // utilisation du bundle faker (génere des données en bdd)
        $faker = Factory::create('fr_FR');
        // Création d'un utilisateur
        $user = new User();

        $user->setEmail('bilel68@gmail.com')
             ->setPrenom('Bilel')
             ->setNom('REHAHLA')
             ->setTelephone('0652148790')
             ->setAPropos('Developpeur Full-Stack,  Comptant plus de 3 années d’expériences 
             en tant que développeur Fullstack que sa soit côté back ou côté Front .
              J’ai pu travailler  dans différent domaine d’ applications  et logiciel. 
             Je possède un vaste savoir-faire en codage et mise en œuvre de solutions technologiques')
             ->setLinkedin('linkedin.com/in/bilel-rehahla-288900143')
             ->setRoles(['ROLE_DEVELOPPEUR']);
        $password = $this->passwordEncoder->encodePassword($user, 'agentAdmin42..');
        $user->setPassword($password);

        $manager->persist($user);

        for ($i = 0; $i < 7; $i++) {
            $blogpost = new Blogpost();
            $blogpost->setTitre($faker->words(3, true))
                     ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                     ->setContenu($faker->text(350))
                     ->setSlug($faker->slug(3))
                     ->setUser($user);
            $manager->persist($blogpost);
        }

        for ($k = 0; $k < 3; $k++) {
            $categorie = new Categorie();
            $categorie->setNom($faker->words(3, true))
                    ->setDescription($faker->text())
                    ->setSlug($faker->slug());
            $manager->persist($categorie);

            for ($j = 0; $j < 2; $j++) {
                $projet = new Projet();
                $projet->setNom($faker->words(3, true))
                       ->setEnCour($faker->randomElement([true, false]))
                       ->setPortfolio($faker->randomElement([true, false]))
                       ->setTarif($faker->randomfloat(2, 20, 60))
                       ->setDuree($faker->dateTimeBetween('-6 month', 'now'))
                       ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                       ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                       ->setDescription($faker->text())
                       ->setSlug($faker->slug())
                       ->addCategorie($categorie)
                       ->setUser($user)
                       ->setFile('/img/imgPC12.jpg');
                $manager->persist($projet);
            }
        }
        $manager->flush();
    }
}
