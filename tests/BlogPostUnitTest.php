<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Blogpost;
use DateTime;


class BlogPostUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $datetime= new DateTime();
        $blogPost = new Blogpost() ;
        $blogPost->setTitre('titre')
        ->setCreatedAt($datetime)
        ->setContenu('contenu')
        ->setSlug('slug');


        $this->assertTrue($blogPost->getTitre() === 'titre');
        $this->assertTrue($blogPost->getCreatedAt() === $datetime);
        $this->assertTrue($blogPost->getContenu() === 'contenu');
        $this->assertTrue($blogPost->getSlug() === 'slug');

    }
    public function testIsFalse(): void
    {
        $datetime= new DateTime();
        $blogPost = new Blogpost() ;
        $blogPost->setTitre('titre')
        ->setCreatedAt($datetime)
        ->setContenu('contenu')
        ->setSlug('slug');

        $this->assertFalse($blogPost->getTitre() === 'false');
        $this->assertFalse($blogPost->getCreatedAt() === new Datetime());
        $this->assertFalse($blogPost->getContenu() === 'false');
        $this->assertFalse($blogPost->getSlug() === 'false');


    }
    public function testIsEmpty(): void
    {
        $blogPost = new Blogpost();

        $this->assertEmpty($blogPost->getTitre());
        $this->assertEmpty($blogPost->getCreatedAt());
        $this->assertEmpty($blogPost->getContenu());
        $this->assertEmpty($blogPost->getSlug());


    }

}
